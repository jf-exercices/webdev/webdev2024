<?php
if (!empty($_SESSION['userid'])) {
    $con = connect();
    // récupération de l'utilisateur correspondant à l'ID de session
    $result = $con->prepare("SELECT * FROM user WHERE id = ?");
    $result->execute([$_SESSION['userid']]);
    $user = $result->fetchObject();
    // vérification de l'existence de l'utilisateur
    if (!is_object($user)) {
        header("HTTP/1.1 401");
        die;
    }
    unset($user->password);
    // output
    $output = '';
    foreach ($user as $key => $value) {
        $output .= '<tr><td>' . $key . '</td><td>' . $value . '</td></tr>';
    }
    if ($output) {
        $output = '<h2>Profil</h2>
                    <table class="table">
                        <thead><tr><th>Intitulé</th>
                                    <th>Donnée</th>
                        </tr></thead>'
            . $output . '</table>';
    }
    manageAlerts();
    echo $output;
}