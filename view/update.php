<?php
    if(!empty($_SESSION['userid'])) {
        $user = getConnectedUser();
        if (!is_object($user)) {
            header("HTTP/1.1 401");
            die;
        }
?>
<h1>Mise à jour du compte</h1>

<form action="index.php?slug=action/update.php" method="post">
    <label for="email">
        Email
    </label>
    <input type="email" id="email" name="email"
           placeholder="<?=$user->email?>"><br>
    <label for="firstname">
        Firstname
    </label>
    <input type="text" id="firstname" name="firstname"
           placeholder="<?=$user->firstname?>"><br>
    <label for="lastname">
        Lastname
    </label>
    <input type="text" id="lastname" name="lastname"
           placeholder="<?=$user->lastname?>"><br>
    <input type="submit" value="mettre à jour">
</form>

<?php
    }
?>