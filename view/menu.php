<header>
    <nav>
        <ul>
            <li><a href="index.php">Accueil</a></li>
            <?php
            if(!empty($_SESSION['userid'])) {
            ?>
                <li><a href="index.php?slug=view/profile.php">Profile</a></li>
                <li><a href="index.php?slug=action/logout.php">Logout</a></li>
                <li><a href="index.php?slug=view/update.php">Update</a></li>
            <?php } else { ?>
                <li><a href="index.php?slug=view/login.html">Login</a></li>
                <li><a href="index.php?slug=view/signup.html">Signup</a></li>
            <?php } ?>
        </ul>
    </nav>
</header>