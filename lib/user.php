<?php

function getConnectedUser(): object|false
{
    if (!empty($_SESSION['userid'])) {
        $con = connect();
        // récupération de l'utilisateur correspondant à l'ID de session
        $result = $con->prepare("SELECT * FROM user WHERE id = ?");
        $result->execute([$_SESSION['userid']]);
        return $result->fetchObject();
    } else {
        return false;
    }
}