<?php


/**
 * @param string $dbname
 * @param string $user
 * @param string $password
 * @param string $host
 * @return PDO
 */
function connect(): PDO {
    return new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST
        , DB_USER, DB_PASSWORD);
}

