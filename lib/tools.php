<?php
/**
 *  Fichier contenant les fonctions utilisées par notre application
 */


/**
 * @param string $data
 * @return string
 */
function cleanData(string $data) :string {
    return strip_tags(trim($data));
}

/**
 * @param array $data
 * @return array
 */
function validDataType(array $data) :array {

    foreach ($data as $key => $value) {
        $data[$key] = cleanData($value);
        if ($key == 'email') {
            $data['email'] = filter_var($value, FILTER_VALIDATE_EMAIL);
//        } elseif($key == 'password') {
//            if (strlen($value) < 8
//                || !preg_match("/[\d]/", $value)
//                || !preg_match("/[a-z]/", $value)
//                || !preg_match("/[A-Z]/", $value)
//                || !preg_match("/[0-9]/", $value)
//                || !preg_match("/[\W]/", $value)
//            ) {
//                $data['password'] = false;
//            }
        } elseif ($key == 'ip') {
            $data['ip'] = filter_var($value, FILTER_VALIDATE_IP);
        } elseif ($key == 'timestamp') {
            $date = new \DateTime();
            $date->setTimestamp($data['timestamp']);
            if ($date != new DateTime()) {
                $data['timestamp'] = false;
            }
        }
    }
    return $data;
}

function getContent(string $slug) {
    if (!empty($slug) && file_exists($slug)) {
        include_once $slug;
    }
}