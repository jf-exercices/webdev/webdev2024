<?php
session_start();
require_once 'config.php';
require_once 'lib/db.php';
require_once 'lib/alert.php';
require_once 'lib/user.php';

include_once 'view/header.html';
include_once 'view/menu.php';
include_once 'view/content.php';
include_once 'view/footer.html';

?>