<?php

/**
 * Copier ce fichier et renommez le en config.php
 * Définissez les valeurs des constantes adéquates
 */

const ROOT_PATH = __DIR__;

const DB_HOST = 'localhost';
const DB_USER = '';
const DB_PASSWORD = '';
const DB_NAME = '';
