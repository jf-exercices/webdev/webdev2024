<?php
/**
 * Script de traitement du formulaire de login (son path doit être la valeur de l'attribut action de la balise form du formulaire)
 *
 * Dans le cas d'un formulaire utilisant la method GET, les données envoyéees par le formulaire seront présentes dans la variable $_GET
 * Dans le cas d'un formulaire utilisant la method POST, les données envoyéees par le formulaire seront présentes dans la variable $_POST
 * Ces 2 variables sont des tableaux (array) associatifs dont le nom est réservé (vous ne pouvez pas créer de variable PHP portant ces noms).
 * Elles sont disponibles dans tous les contextes du script (portée globale), et donc appelées en PHP "superglobales".
 *
 */

// Afin de vérifier que votre formulaire envoie bien les données du formulaire au script, vous pouvez utiliser la fonction native var_dump() en début de script :
// var_dump($_POST); die;

/**
 *  Dans le but de rendre le code d'une application modulaire et plus maintenable, on l'organiser dans différents fichiers.
 *  Afin de pouvoir utiliser le code contenu dans un autre script, il convient d'utiliser les instructions de langage d'inclusion de fichier.
 *
 *  En PHP, il en existe plusieurs dont le rôle est spécifique :
 *  - "include" permet d'inclure ET exécuter le contenu du script passé en paramètre, à l'endroit où il est appelé.
 *  - "include_once" a la même fonction que "include" mais la différence est que si le code a déjà été inclus, il ne le sera pas une seconde fois.
 *  - "require" est identique à "include" mis à part le fait que lorsqu'une erreur survient, il produit également une erreur fatale, et donc l'arrêt du script.
 *  - "require_once"a la même fonction que "require" mais la différence est que si le code a déjà été inclus, il ne le sera pas une seconde fois.
 *
 *  Le paramètre contiendra le PATH du fichier à inclure.
 *  PHP possède des "constantes magiques" permettant de faciliter l'écriture des paths :
 *  __FILE__
 *  Le chemin complet et le nom du fichier courant avec les liens symboliques résolus.
 *  Si utilisé pour une inclusion, le nom du fichier inclus est retourné.
 *  __DIR__
 *  Le dossier du fichier. Si utilisé dans une inclusion, le dossier du fichier inclus sera retourné.
 *  Ce nom de dossier ne contiendra pas de slash final, sauf si c'est le dossier racine.
 *
 */

// Dans ce cas on utilise REQUIRE car le script "lib/tools.php" contient la déclaration de la fonction cleanData utilisée dans ce script.
require_once ROOT_PATH . '/lib/tools.php';
// Dans ce cas, on utilise INCLUDE_ONCE car l'échec de l'affichage ne doit pas empêcher l'exécution du script et on s'assure qu'il ne s'affiche qu'une seule fois
include_once ROOT_PATH . '/view/welcome.html';

// Boucle foreach parcourant le tableau $_POST afin d'en extraire les clés et les valeurs et de les afficher.
// L'affichage des valeurs fera appel à la fonction cleanData présente dans le fichier lib/tools.php
foreach ($_POST as $key => $value) {
    echo $key . ' : ' . cleanData($value) . '<br>';
}