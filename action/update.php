<?php

if(!empty($_SESSION['userid'])) {
    if (empty($_POST)) {
        header("HTTP/1.1 405");
        die;
    }
    // vérification de l'existence de l'utilisateur
    $user = getConnectedUser();
    if (!is_object($user)) {
        header("HTTP/1.1 401");
        die;
    }
    // les champs à mettre à jour doivent être valides
    $data = validDataType($_POST);
    // vérification des champs à mettre à jour
    // & construction de la requête SQL
    $dbFields = ['firstname', 'lastname', 'email'];
    $params = [];
    $sql = '';
    foreach ($data as $key => $value) {
        // on vérifie si la valeur n'est pas vide
        // et si la clé fait partie des champs en DB
        if (!empty($value) && in_array($key, $dbFields)) {
            // construction de la requête SQL (SET fields)
            $sql .= "$key = ?,";
            // paramètres de la requête
            $params[] = $value;
        }
    }
    // dernier charactère supprimé si c'est une virgule (syntaxe SQL)
    $sql = trim($sql, ',');
    if ($sql) {
        // ajout de l'ID aux params
        $params[] = $user->id;
        // mise à jour des champs
        $con = connect();
        $update = $con->prepare("UPDATE user SET $sql WHERE id = ?");
        $update->execute($params);
        // vérification de la mise à jour
        if ($update->rowCount()) {
            createAlert("L'utilisateur $user->email a été mise à jour"
                , 'success', 'index.php?slug=view/profile.php');
        } else {
            createAlert("L'utilisateur n'a pas été mise à jour"
                , 'danger', 'index.php?slug=view/update.php');
        }
    }
    $redirect = 'Location: index.php?slug=view/message.php';
} else {
    $redirect = "HTTP/1.1 401";
}
header($redirect);
die;