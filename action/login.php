<?php

// le formulaire ne doit pas être vide
if (empty($_POST)) {
    header("HTTP/1.1 405");
    die;
}

// l'email doit être valide
$data = validDataType($_POST);

// connexion à la DB
$con = connect();
// requête SQL
$result = $con->prepare("SELECT * FROM user WHERE email = ?");
// Exécution de la requête avec le paramètre
$result->execute([$data['email']]);
// FETCH (transformation du résultat de la requête en objet PHP)
$user = $result->fetchObject();
// si l'utilisateur n'existe pas, une erreur est renvoyée
if (!is_object($user)) {
    header("HTTP/1.1 401");
    die;
}

// comparaison entre le mot de passe du formulaire et le mot de passe crypté en DB
if (password_verify($data['password'], $user->password)) {
    //update user->updated
    $update = $con->prepare("UPDATE user SET updated = NOW() WHERE id = ?");
    $update->execute([$user->id]);
    $_SESSION['userid'] = $user->id;
    createAlert('Bonjour ' . $user->firstname, 'success', 'index.php?slug=view/profile.php');
} else {
    header("HTTP/1.1 403");
}
die;
