<?php

/**
 * 1e étape :
 *  Créez un script permettant de récupérer les données du formulaire de signup et affichant, dans un tableau HTML, chaque donnée avec son intitulé et sa valeur
 *
 * 2e étape :
 *  Utilisez la fonction validDataType() sur chaque donnée envoyée par le formulaire.
 *
 * 3e étape :
 *  Inclure la date d'inscription
 *  Aborder la superglobale $_SERVER afin de récupérer l'adresse IP
 *
 */

/*
 * INPUT
 */
require_once ROOT_PATH . '/lib/tools.php';

$output = '';

/*
 * INSTRUCTIONS
 */
if (empty($_POST)) {
    header("HTTP/1.1 405");
    die;
}

$data = validDataType($_POST);

// insérer les données en base de données

// connexion à la DB
$dbh = connect();
// Requête SQL où les paramètres sont remplacés par des ?
$query = "INSERT INTO user 
            (firstname, lastname, email, password, ip, status, created) 
            VALUES (?, ?, ?, ?, ?, 0, NOW())";
// données à passer à la requête, sous forme de tableau
$params = [
    $data['f-firstname'],
    $data['f-lastname'],
    $data['f-email'],
    password_hash($data['f-password'], PASSWORD_DEFAULT), //cryptage
    $_SERVER['REMOTE_ADDR'],
];
// envoi de la requête via la méthode PREPARE
// la requête SQL doit être passée en paramètre
$sql = $dbh->prepare($query);
// exécution de la requête via la méthode EXECUTE
// les données doivent être passés en paramètre
$sql->execute($params);
$userid = $dbh->lastInsertId();
if (!empty($userid)) {
    echo "Vous avez créé un compte avec l'identifiant " . $userid . PHP_EOL;
}

foreach ($data as $key => $value) {
    $output .= '<tr><td>' . $key . '</td><td>' . $value . '</td></tr>';
}

/*
 * OUTPUT
 */
echo '
<table>
    <tr>
        <th>Intitulé</th>
        <th>Valeur</th>
    </tr>
    ' . $output . '
</table>
';


?>
